<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class MahasiswaController extends Controller
{
    public function index()
    {
        return view('module.mahasiswa.index');
    }

    public function show()
    {
        return view('module.mahasiswa.detail');
    }
}
